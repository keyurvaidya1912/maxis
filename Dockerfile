FROM openjdk:11-jdk
COPY build/libs/app-0.0.1-SNAPSHOT.jar app.jar
ENTRYPOINT ["sh","-c","java -jar app.jar"]