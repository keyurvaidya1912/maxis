# App

## Run

```
./gradlew clean bootRun
```

## Test

```
curl "localhost:8080/echo?echo=hello"
{"echo":"ping"}
```
